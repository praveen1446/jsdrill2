function findCityAndCountry(arrayOfObjects){
    const cityAndCountryList=[];
    for(let index=0;index<arrayOfObjects.length;index++){
        cityAndCountryList.push(arrayOfObjects[index].city,arrayOfObjects[index].country);
    }
    return cityAndCountryList;
}
module.exports=findCityAndCountry;

