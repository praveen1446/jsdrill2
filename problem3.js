
function findStudents(arrayOfObjects){
    const  studentsList=[];
    for(let index =0;index<arrayOfObjects.length;index++){
        if(arrayOfObjects[index].isStudent === true  &&  arrayOfObjects[index].country === 'Australia'){
            studentsList.push(arrayOfObjects[index].name);
        }
    }
    return studentsList;
}
module.exports=findStudents
